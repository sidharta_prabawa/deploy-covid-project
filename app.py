import keras
import numpy as np
from flask import Flask, render_template, request

from PIL import Image
from keras.preprocessing import image
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.models import load_model

app = Flask(__name__)
model = load_model('kemal_covid_model.h5')

@app.route('/', methods=['GET'])
def hello_world():
    return render_template("index.html")

@app.route('/', methods=['POST'])
def predict():
    imagefile = request.files['imagefile']
    
    image_path = "./images/" +imagefile.filename
    imagefile.save(image_path)

    img=image.load_img(image_path, target_size=(224, 224))
  
    x=image.img_to_array(img)
    x=np.expand_dims(x, axis=0)
    images = np.vstack([x])
  
    classes = model.predict(images, batch_size=10)

    print("RESULT IMAGE : ")
    if classes[0]>0:
        result = imagefile.filename + " is a Normal"
        print(imagefile.filename + " is a Normal")
        
    else:
        result = imagefile.filename + " is a Covid"
        print(imagefile.filename + " is a Covid")

    return render_template("index.html", prediction=result)

if __name__ == '__main__':
    app.run(port=3000, debug=True)